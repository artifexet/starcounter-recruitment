### Info

As a developer, I focused mainly on js code. I assume you already have ready css styles for basic components so I just set only layout.
I do not have an iphone, so I tested app in safari browser on osx.

If you have comments, please contact me:

Szymon Rybka

---

Email: [artifexet@gmail.com](mailtto:artifexet@gmail.com)

Tel: 661-525-250


### Questions to product owner
* ask for acceptance criteria
* what men `Space to grow for more input fields`

### Source code

Demo is stored in [app folder](app).

### Mock

![](doc/mock.png)