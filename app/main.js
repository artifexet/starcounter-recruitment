const Controller = function (userData, viewElement) {
    const emailFormTemplateClassName = 'email-form-template';

    let userNameInput,
        userLastNameInput,
        userPhotoImage,
        personNameElement,
        emailFormContainerElement,
        addEmail,
        saveElement,
        cancelElement,
        messageContainerElement,
        emailFormTemplateElement;

    function init() {
        initEmailFormTemplates();
        initViewElements();
        initListeners();
        updateView();
    }

    function initEmailFormTemplates() {
        emailFormTemplateElement = viewElement.getElementsByClassName(emailFormTemplateClassName)[0];
        emailFormTemplateElement.parentNode.removeChild(emailFormTemplateElement);
    }

    function getElementByClassName(className) {
        return viewElement.getElementsByClassName(className)[0];
    }

    function initViewElements() {
        userNameInput = getElementByClassName('person-form--first-name');
        userLastNameInput = getElementByClassName('person-form--last-name');
        userPhotoImage = getElementByClassName('user-photo');
        personNameElement = getElementByClassName('person-name');
        emailFormContainerElement = getElementByClassName('email-form-container');
        addEmail = getElementByClassName('email-form--add-email');
        saveElement = getElementByClassName('save-action');
        cancelElement = getElementByClassName('cancel-action');
        messageContainerElement = getElementByClassName('message-container');
    }

    function initListeners() {
        addEmail.addEventListener('click', handleAddEmail);
        saveElement.addEventListener('click', handleSave);
        cancelElement.addEventListener('click', handleCancel);
    }

    function updateView() {
        removeError();
        personNameElement.innerText = `${userData.name} ${userData.lastName}`;
        userNameInput.value = userData.name;
        userLastNameInput.value = userData.lastName;
        userPhotoImage.src = userData.photoUrl;

        clearEmailsContainer();
        userData.emails.forEach((emailData) => {
            addEmailForm(emailData, false);
        });
    }

    function clearEmailsContainer() {
        while (emailFormContainerElement.firstChild) {
            emailFormContainerElement.removeChild(emailFormContainerElement.firstChild);
        }
    }

    function addEmailForm(emailData, animate = true) {
        const node = emailFormTemplateElement.cloneNode(true);
        if (emailData) {
            const roleInput = node.getElementsByClassName('email-form--role')[0];
            const emailInput = node.getElementsByClassName('email-form--email')[0];
            emailInput.value = emailData.email;
            roleInput.value = emailData.role;
        }
        const removeElement = node.getElementsByClassName('email-form--remove')[0];
        const removeFunc = () => {
            setTimeout(()=> {
                node.parentNode.removeChild(node);
            }, 1000);
            node.style.opacity = "0";
            removeElement.removeEventListener('click', removeFunc);
        };
        removeElement.addEventListener('click', removeFunc);

        emailFormContainerElement.appendChild(node);
        if (animate) {
            setTimeout(()=> {
                node.style.opacity = "1";
            }, 0);
        } else {
            node.style.opacity = "1";
        }
    }

    function handleAddEmail() {
        addEmailForm();
    }

    function handleSave() {
        if (validateForm()) {
            const emailForms = viewElement.getElementsByClassName(emailFormTemplateClassName);
            const emailsData = [];

            for (let i = 0; i <emailForms.length ; i++) {
                const emailTemplateElement = emailForms[i];
                const roleInput = emailTemplateElement.getElementsByClassName('email-form--role')[0];
                const emailInput = emailTemplateElement.getElementsByClassName('email-form--email')[0];

                emailsData.push({
                    email: emailInput.value,
                    role: roleInput.value,
                });
            }

            const data = {
                name: userNameInput.value,
                lastName: userLastNameInput.value,
                emails: emailsData,
            };

            userData = Object.assign(userData, data);

            updateView();
            console.log('saved', userData);
        } else {
            showError("Please fill required element");

        }

    }

    function handleCancel() {
        updateView();
    }

    function validateForm() {
        const invalidInputs = viewElement.querySelectorAll('input[required]:invalid');

        return invalidInputs.length == 0;
    }

    function removeError() {
        messageContainerElement.innerText = "";
    }

    function showError(text) {
        messageContainerElement.innerText = text;
    }

    init();
};

const demoUserData = {
    name: 'Szymon',
    lastName: 'Rybka',
    photoUrl: 'user-photos/szymon-rybka-photo.jpg',
    emails: [
        {
            email: 'artifexet@gmail.com',
            role: 'Home'
        },
        {
            email: 'biuro@srconsilting.pl',
            role: 'Work'
        },
        {
            email: 'spam@srconsilting.pl',
            role: 'Spam'
        }
    ]
};

const view = document.getElementsByClassName('personal-edit-view')[0];

new Controller(demoUserData, view);